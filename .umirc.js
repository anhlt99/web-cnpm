
// ref: https://umijs.org/config/
export default {
    treeShaking: true,
    routes: [
        {
            path: '/',
            component: '../layouts/index',
            routes: [
                {
                    path: '/',
                    component: '../pages/index',
                    routes: [
                        { path: '/dangnhap', component: '../pages/Login/index' },
                        { path: '/dangky', component: '../pages/Register/index' }
                    ]
                },
                {
                    path: '/',
                    component: '../pages/index',
                    routes: [
                        { path: '/dangnhap', component: '../pages/Login/index' },
                        { path: '/dangky', component: '../pages/Register/index' }
                    ]
                },
            ]
        },

    ],
    plugins: [
        // ref: https://umijs.org/plugin/umi-plugin-react.html
        ['umi-plugin-react', {
            antd: true,
            dva: true,
            dynamicImport: false,
            title: 'My Web',
            dll: false,

            routes: {
                exclude: [
                    /models\//,
                    /services\//,
                    /model\.(t|j)sx?$/,
                    /service\.(t|j)sx?$/,
                    /components\//,
                ],
            },
        }],
    ],
}
