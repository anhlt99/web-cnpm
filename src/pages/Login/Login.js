import { Button, Checkbox, Form, Icon, Input, Modal } from 'antd';
import { connect } from 'dva';
import React from 'react';
import router from 'umi/router';
@connect(({ login }) => ({
    login
}))
class Login extends React.Component {
    handleCancel = () => {
        this.props.dispatch({
            type: 'login/changeState',
            payload: {
                visible: false
            },
        });
    };
    handleClickRegister = () => {
        router.push("/dangky")
        this.props.dispatch({
            type: 'login/changeState',
            payload: {
                visibleRegister: true,
                visible: false
            },
        });
    };
    render() {
        const { login: { visible } } = this.props;
        const { getFieldDecorator } = this.props.form;
        return (
            <Modal visible={visible} onCancel={this.handleCancel}>
                <div id="wrap-form-login">
                    <div className="wrap-login">
                        <div className="close-login">
                            <h1>ĐĂNG NHẬP</h1>
                        </div>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <Form.Item>
                                {getFieldDecorator('username', {
                                    rules: [{ required: true, message: 'Please input your username!' }],
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        placeholder="Username"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your Password!' }],
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        type="password"
                                        placeholder="Password"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('remember', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                                })(<Checkbox>Remember me</Checkbox>)}
                                <a className="login-form-forgot" href="">
                                    Forgot password
                                </a>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Log in
                                </Button>
                                Or <a onClick={() => { this.handleClickRegister() }}>register now!</a>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </Modal>
        );
    }
}
export default Form.create({ name: 'normal_login' })(Login);
