import React from 'react';
import Content from './Content/index';


// eslint-disable-next-line react/prefer-stateless-function
class App extends React.Component {
  render() {
    return (
      <div className="content">
        <Content />

      </div>
    )
  }
}
export default App;
