import React from 'react'
import NamHoc from './Namhoc'
import imgNam1 from './imgNamHoc/icon-1.png'
import imgNam2 from './imgNamHoc/icon-2.png'
import imgNam3 from './imgNamHoc/icon-3.png'
import imgNam4 from './imgNamHoc/icon-4.png'
import { Carousel } from 'antd'
import EbookMoi from '../EbookMoi/EbookMoi'

class totalNamHoc extends React.Component {
    render() {
        var datanamhoc = [
            {
                name: 'NĂM 1',
                image: imgNam1,
                status: true
            },
            {
                name: 'NĂM 2',
                image: imgNam2,
                status: true
            },
            {
                name: 'NĂM 3',
                image: imgNam3,
                status: true
            },
            {
                name: 'NĂM 4',
                image: imgNam4,
                status: true
            }
        ]
        let allNamHoc = datanamhoc.map((datanamhoc) => {
            let result = '';
            if (datanamhoc.status) {
                return <NamHoc srcImageNamHoc={datanamhoc.image} tenNamHoc={datanamhoc.name}>
                </NamHoc>
            }
            return result;
        });
        const y = (
            <div style={{ display: 'flex', marginTop: 40 }}>
                <div>
                    {allNamHoc[0]}
                </div>
                <div>
                    {allNamHoc[1]}
                </div>
                <div>
                    {allNamHoc[2]}
                </div>
                <div>
                    {allNamHoc[3]}
                </div>
            </div>
        );
        return (
            <div className="test">
                <Carousel autoplay>
                    <div >
                        <h1 style={{ marginTop: 15, fontWeight: 600 }}>Tài Liệu</h1>
                        {y}
                    </div>
                    <div>
                        <EbookMoi />
                    </div>
                </Carousel>
            </div>
        )
    }
}
export default totalNamHoc;