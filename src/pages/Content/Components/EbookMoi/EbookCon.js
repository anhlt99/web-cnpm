import React from 'react'
import { Icon } from 'antd';
class EbookCon extends React.Component {
    render() {
        const { handleClick, srcImageEbook, textEbook, countClick } = this.props;
        return (
            <div onClick={handleClick} className="wrap-col-ebookcon">
                <div className="col-ebookcon">
                    <div>
                        <img src={srcImageEbook} style={{ width: '160px', height: 200, paddingTop: 2 }} />
                    </div>
                    <div className="wrap-text-ebook">
                        <h5 className="text-ebook">{textEbook}</h5>
                        <div className="wrap-icon-countClick">
                            <Icon type="eye"></Icon>
                            <div style={{ marginLeft: 3, marginTop: -3 }}>{countClick}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default EbookCon;