import React from 'react'
import { Carousel } from 'antd';
import EbookCon from './EbookCon';
import router from 'umi/router';
class EbookMoi extends React.Component {
    handleClickTaiLieu = () => {
        router.push("/tailieu1")

    };
    render() {
        var EbookMoi = [
            {
                name: 'Lippincott Illustrated Reviews Biochemistry 7th Edition 2017',
                image: 'http://luutru.vietlib.vn/upload/4b2553ebc1e2ecd65c820887d0e998a9/[vietlib.vn]-lippincott-illustrated-reviews-biochemistry-7th-edition-2017.jpg',
                countClick: 9,
                status: true
            },
            {
                name: 'The Hitchhikers Guide to the Galaxy',
                image: 'http://luutru.vietlib.vn/upload/c488959a2511411b94bb1f0bf05d651f/[vietlib.vn]-hitchhikers-guide-to-the-galaxy-the---douglas-adams--eoin-colfer.jpg',
                countClick: 1,
                status: true
            },
            {
                name: 'Watership Down',
                image: 'http://luutru.vietlib.vn/upload/fad59b3e1f2211845d0ac5933f28fb1e/[vietlib.vn]-watership-down---richard-adams.jpg',
                countClick: 3,
                status: true
            },
            {
                name: 'Little Women',
                image: 'http://luutru.vietlib.vn/upload/af3f28684eef95f740bdc3f1114e85da/[vietlib.vn]-little-women---louisa-may-alcott.jpg',
                countClick: 3,
                status: true
            },
            {
                name: 'Interview With the Vampire',
                image: 'http://luutru.vietlib.vn/upload/1b5c1ae4cf5e02ece8effef64af1c502/[vietlib.vn]-interview-with-the-vampire---anne-rice.jpg',
                countClick: 4,
                status: true
            }
        ]
        let element = EbookMoi.map((EbookMoi) => {
            let result = '';
            if (EbookMoi.status) {
                return <EbookCon textEbook={EbookMoi.name} srcImageEbook={EbookMoi.image} countClick={EbookMoi.countClick}>
                </EbookCon>
            }
            return result;
        });
        const x = (
            <div style={{ display: 'flex' }}>
                <a onClick={() => { this.handleClickTaiLieu() }}>
                    {element[0]}
                </a>
                <a>
                    {element[1]}
                </a>
                <a>
                    {element[2]}
                </a>
                <a>
                    {element[3]}
                </a>
                <a>
                    {element[4]}
                </a>
            </div>
        )
        return (
            <div>
                <div>
                    <h1 style={{ marginTop: 15, fontWeight: 600 }}>Ebook mới</h1>
                    {x}
                </div>
            </div>
        );
    }
}
export default (EbookMoi);