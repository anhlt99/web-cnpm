export default {
    namespace: 'login',
    state: {
        visible: false,
        visibleRegister: false,
    },
    reducers: {
        changeState(state, { payload }) {
            return {
                ...state,
                ...payload,
            };
        },
    },
};