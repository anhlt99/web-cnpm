import { createLogger } from 'redux-logger';

export const dva = {
  config: {
    onError(err) {
      err.preventDefault();
      // eslint-disable-next-line no-console
      console.error(err.message);
    },
    onAction: createLogger(),
  },
};
