import { Button, Dropdown, Input, Menu, message, Select } from 'antd';
import { connect } from 'dva';
import React from 'react';
import Login from '../pages/Login/Login';
import ImageLogo from '../assets/logo.png';
import ImageEbook from '../assets/ebook.png';
import ImagePen from '../assets/pen.png';
import router from 'umi/router';
import Register from '../pages/Register/index'
const { SubMenu } = Menu;
const InputGroup = Input.Group;
const { Option } = Select;
const { Search } = Input;
@connect(({ login }) => ({
    login
}))
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 'mail',
            ebook: ImageEbook,
            logo: ImageLogo,
            pen: ImagePen,
        };
    }
    handleClickLogin = () => {
        router.push('/dangnhap')
        this.props.dispatch({
            type: 'login/changeState',
            payload: {
                visible: true
            },
        });
    }
    handleClickRegister = () => {
        router.push('/dangky')
        debugger
        this.props.dispatch({
            type: 'login/changeState',
            payload: {
                visibleRegister: true,
                visible: false
            },
        });
    }
    onClick = ({ key }) => {
        message.info(`Click on item ${key}`);
    };

    handleClick = e => {
        console.log('click ', e);
        this.setState({
            current: e.key,
        });
    };
    menu = (
        <Menu onClick={this.onClick}>
            <Menu.Item key="1">Tài liệu</Menu.Item>
            <Menu.Item key="2">Ebook</Menu.Item>
            <Menu.Item key="3">Đề thi</Menu.Item>
        </Menu>
    );
    state = {
        dataSource: [],
    };

    handleChange = value => {
        this.setState({
            dataSource:
                !value || value.indexOf('@') >= 0
                    ? []
                    : [`${value}@gmail.com`, `${value}@163.com`, `${value}@qq.com`],
        });
    };

    render() {
        const { login: { visible, visibleRegister } } = this.props;
        return (
            <div>
                <div id='menu'>
                    <div className="wrap-menu">
                        <Menu className="menu-header" onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
                            <Menu.Item className="menu-logo">
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <img src={[this.state.logo]} />
                                    <div className="text">VietLib</div>
                                </div>
                            </Menu.Item>
                            <SubMenu className="hover-menu"
                                title={
                                    <span className="submenu-title-wrapper">
                                        <img src={this.state.pen} />
                                        <div className="text">Tài Liệu</div>
                                    </span>
                                }
                            >
                                <Menu.ItemGroup title="Năm 1">
                                    <SubMenu key="sub1" title="Kỳ 1">
                                        <SubMenu key="sub2" title="Kỹ thuật">
                                            <Menu.Item key="setting:1">THCS1</Menu.Item>
                                            <Menu.Item key="setting:2">Giải tích 1</Menu.Item>
                                            <Menu.Item key="setting:3">Đại số</Menu.Item>
                                            <Menu.Item key="setting:4">Mác 1</Menu.Item>
                                            <Menu.Item key="setting:5">Mác 2</Menu.Item>
                                        </SubMenu>
                                        <SubMenu key="sub3" title="Kinh tế">
                                            <Menu.Item key="setting:1">THCS1</Menu.Item>
                                            <Menu.Item key="setting:2">Toán cao cấp 1</Menu.Item>
                                            <Menu.Item key="setting:3">Đại số</Menu.Item>
                                            <Menu.Item key="setting:4">Mác 1</Menu.Item>
                                        </SubMenu>
                                    </SubMenu>
                                    <SubMenu key="sub4" title="Kỳ 2">
                                        <Menu.Item key="setting:2">THCS1</Menu.Item>
                                    </SubMenu>
                                </Menu.ItemGroup>
                                <Menu.ItemGroup title="Năm 2">
                                    <SubMenu key="sub1" title="Kỳ 1">
                                        <SubMenu key="sub2" title="Kỹ thuật">
                                            <Menu.Item key="setting:1">THCS1</Menu.Item>
                                            <Menu.Item key="setting:2">Giải tích 1</Menu.Item>
                                            <Menu.Item key="setting:3">Đại số</Menu.Item>
                                            <Menu.Item key="setting:4">Mác 1</Menu.Item>
                                        </SubMenu>
                                        <SubMenu key="sub3" title="Kinh tế">
                                            <Menu.Item key="setting:1">THCS1</Menu.Item>
                                            <Menu.Item key="setting:2">Toán cao cấp 1</Menu.Item>
                                            <Menu.Item key="setting:3">Đại số</Menu.Item>
                                            <Menu.Item key="setting:4">Mác 1</Menu.Item>
                                        </SubMenu>
                                    </SubMenu>
                                    <SubMenu key="sub4" title="Kỳ 2">
                                        <Menu.Item key="setting:2">THCS1</Menu.Item>
                                    </SubMenu>
                                </Menu.ItemGroup>
                            </SubMenu>
                            <SubMenu className="hover-menu"
                                title={
                                    <span className="submenu-title-wrapper">
                                        <img src={this.state.ebook} />
                                        <div className="text">Ebook</div>
                                    </span>
                                }
                            >
                                <Menu.ItemGroup title="Item 1">
                                    <Menu.Item key="setting:1">Option 1</Menu.Item>
                                    <Menu.Item key="setting:2">Option 2</Menu.Item>
                                </Menu.ItemGroup>
                                <Menu.ItemGroup title="Item 2">
                                    <Menu.Item key="setting:3">Option 3</Menu.Item>
                                    <Menu.Item key="setting:4">Option 4</Menu.Item>
                                </Menu.ItemGroup>
                            </SubMenu>
                            <SubMenu className="hover-menu"
                                title={
                                    <span className="submenu-title-wrapper">
                                        <img src={this.state.ebook} />
                                        <div className="text">Đề thi</div>
                                    </span>
                                }
                            >
                                <Menu.ItemGroup title="Item 1">
                                    <Menu.Item key="setting:1">Option 1</Menu.Item>
                                    <Menu.Item key="setting:2">Option 2</Menu.Item>
                                </Menu.ItemGroup>
                                <Menu.ItemGroup title="Item 2">
                                    <Menu.Item key="setting:3">Option 3</Menu.Item>
                                    <Menu.Item key="setting:4">Option 4</Menu.Item>
                                </Menu.ItemGroup>
                            </SubMenu>
                        </Menu>
                        <div style={{ alignItems: 'center', margin: '0 10px', width: 400 }}>
                            <InputGroup compact style={{ display: 'flex', fontFamily: "'Roboto', sans-serif" }}>
                                <Select defaultValue="Option1">
                                    <Option value="Option1">Tài liệu</Option>
                                    <Option value="Option2">Ebook</Option>
                                    <Option value="Option2">Đề thi</Option>
                                </Select>
                                <Search placeholder="input search text" onSearch={value => console.log(value)} enterButton />
                            </InputGroup>
                        </div>
                        <div class="rs-login-upload">
                            <Button type="normal" className="login" onClick={() => { this.handleClickLogin() }}>Đăng nhập</Button>
                            <Button type="normal" className="register" onClick={() => { this.handleClickRegister() }}>Đăng ký</Button>
                            <div className="upload">
                                <Dropdown overlay={this.menu} >
                                    <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                        UpLoad
                            </a>
                                </Dropdown>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    {visible ? <Login /> : ""}
                    {visibleRegister ? <Register /> : ''}
                </div>
            </div>
        )
    }

}
export default Header;